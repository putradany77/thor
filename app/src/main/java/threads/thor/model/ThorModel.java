package threads.thor.model;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import threads.thor.core.books.BOOKS;
import threads.thor.core.books.Bookmark;
import threads.thor.core.books.BookmarkDatabase;
import threads.thor.core.tabs.TABS;
import threads.thor.core.tabs.Tab;
import threads.thor.core.tabs.TabsDatabase;

public class ThorModel extends AndroidViewModel {
    private final TabsDatabase tabsDatabase;
    private final BookmarkDatabase bookmarkDatabase;
    @NonNull
    private final MutableLiveData<Uri> uri = new MutableLiveData<>(null);

    @NonNull
    private final MutableLiveData<Boolean> online = new MutableLiveData<>(null);

    public ThorModel(@NonNull Application application) {
        super(application);
        tabsDatabase = TABS.getInstance(
                application.getApplicationContext()).getTabsDatabase();
        bookmarkDatabase = BOOKS.getInstance(
                application.getApplicationContext()).getBookmarkDatabase();
    }

    public LiveData<List<Bookmark>> getBookmarks() {
        return bookmarkDatabase.bookmarkDao().getLiveDataBookmarks();
    }

    public LiveData<List<Tab>> getTabs() {
        return tabsDatabase.tabDao().getLiveDataTabs();
    }

    @NonNull
    public MutableLiveData<Uri> uri() {
        return uri;
    }

    public void uri(Uri uri) {
        uri().postValue(uri);
    }

    @NonNull
    public MutableLiveData<Boolean> online() {
        return online;
    }

    public void online(boolean online) {
        online().postValue(online);
    }
}
