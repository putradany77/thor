package threads.thor.core.books;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import threads.thor.core.DOCS;

public class BOOKS {

    private static volatile BOOKS INSTANCE = null;
    private final BookmarkDatabase bookmarkDatabase;


    private BOOKS(BookmarkDatabase bookmarkDatabase) {
        this.bookmarkDatabase = bookmarkDatabase;
    }

    @NonNull
    private static BOOKS createBooks(@NonNull BookmarkDatabase bookmarkDatabase) {
        return new BOOKS(bookmarkDatabase);
    }

    public static BOOKS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (BOOKS.class) {
                if (INSTANCE == null) {
                    BookmarkDatabase pageDatabase = Room.databaseBuilder(context,
                                    BookmarkDatabase.class,
                                    BookmarkDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().
                            build();

                    INSTANCE = BOOKS.createBooks(pageDatabase);
                }
            }
        }
        return INSTANCE;
    }


    @NonNull
    public static Bookmark createBookmark(@NonNull String uri, @NonNull String title, @Nullable Bitmap bitmap) {
        return new Bookmark(uri, title, System.currentTimeMillis(), DOCS.bytes(bitmap), null);
    }


    public void storeBookmark(@NonNull Bookmark bookmark) {
        bookmarkDatabase.bookmarkDao().insertBookmark(bookmark);
    }

    @NonNull
    public BookmarkDatabase getBookmarkDatabase() {
        return bookmarkDatabase;
    }

    @Nullable
    public Bookmark getBookmark(@NonNull String uri) {
        return bookmarkDatabase.bookmarkDao().getBookmark(uri);
    }

    public boolean hasBookmark(@NonNull String uri) {
        return getBookmark(uri) != null;
    }

    public void removeBookmark(@NonNull Bookmark bookmark) {
        bookmarkDatabase.bookmarkDao().removeBookmark(bookmark);
    }

    public void storeDnsLink(@NonNull String uri, @NonNull String link) {
        bookmarkDatabase.bookmarkDao().setDnsLink(uri, link);
    }

    @Nullable
    public String getDnsLink(@NonNull String uri) {
        return bookmarkDatabase.bookmarkDao().getDnsLink(uri);
    }
}
