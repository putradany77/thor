package threads.thor.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.Objects;

import threads.thor.InitApplication;
import threads.thor.R;
import threads.thor.core.DOCS;

public class InfoFragment extends DialogFragment {

    public static final String TAG = InfoFragment.class.getSimpleName();
    public static final String URI = "uri";
    public static final String TITLE = "title";
    public static final String ICON = "icon";

    public static InfoFragment newInstance(String uri, String title, @Nullable Bitmap bitmap) {
        InfoFragment fragment = new InfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(URI, uri);
        bundle.putString(TITLE, title);
        if (bitmap != null) {
            bundle.putParcelable(ICON, bitmap);
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_info, container, false);

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        String uri = args.getString(URI);
        String title = args.getString(TITLE);
        Bitmap icon = args.getParcelable(ICON);

        ImageView info_icon = view.findViewById(R.id.info_icon);
        ImageView imageView = view.findViewById(R.id.info_code);
        TextView titleView = view.findViewById(R.id.info_title);
        titleView.setText(title);
        if (icon != null) {
            info_icon.setVisibility(View.VISIBLE);
            info_icon.setImageBitmap(icon);
        } else {
            info_icon.setVisibility(View.GONE);
        }
        Objects.requireNonNull(uri);
        TextView page = view.findViewById(R.id.info_page);
        if (uri.isEmpty()) {
            page.setVisibility(View.GONE);
        } else {
            page.setText(uri);
        }

        boolean isHomepage = Objects.equals(InitApplication.getHomepage(requireContext()), uri);

        CheckBox homepage = view.findViewById(R.id.info_homepage);
        homepage.setChecked(isHomepage);
        homepage.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                InitApplication.setHomepage(requireContext(), uri);
            } else {
                InitApplication.setHomepage(requireContext(), null);
            }
        });

        Bitmap bitmap = DOCS.getQRCode(uri);
        imageView.setImageBitmap(bitmap);
        return view;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = new BottomSheetDialog(requireContext());
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);
        return dialog;
    }

}
