package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import record.pb.EnvelopeOuterClass;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Record;
import threads.lite.core.Page;
import threads.lite.core.Progress;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;
import threads.lite.quic.Settings;

@RunWith(AndroidJUnit4.class)
public class IpfsAlpnTest {


    private static final String TAG = IpfsAlpnTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void alpn_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        assertEquals(server.numServerConnections(), 0);

        try (Session session = ipfs.createSession()) {
            Dummy dummy = Dummy.getInstance(context);

            PeerId host = ipfs.self();
            assertNotNull(host);
            Multiaddr multiaddr = Multiaddr.getLoopbackAddress(ipfs.self(), server.getPort());


            try (Session dummySession = dummy.createSession()) {


                byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

                Cid cid = IPFS.storeData(session, input);
                assertNotNull(cid);

                byte[] cmp = IPFS.getData(session, cid, () -> false);
                assertArrayEquals(input, cmp);

                List<Cid> cids = IPFS.getBlocks(session, cid);
                LogUtils.debug(TAG, "Links " + cids.size());


                Parameters parameters = Parameters.create(Settings.LITE_ALPN, false);
                Connection connection = dummySession.dial(multiaddr, parameters);
                Objects.requireNonNull(connection);

                Thread.sleep(1000);

                assertEquals(server.numServerConnections(), 1);


                // simple push test
                byte[] test = "moin".getBytes(StandardCharsets.UTF_8);
                EnvelopeOuterClass.Envelope data =
                        IPFS.createEnvelope(dummySession, Record.LITE, test);

                AtomicBoolean notified = new AtomicBoolean(false);
                ipfs.setIncomingPush(push -> {
                    EnvelopeOuterClass.Envelope envelope = push.envelope();
                    notified.set(Arrays.equals(envelope.getPayload().toByteArray(), test));
                });

                IPFS.push(connection, data);

                Thread.sleep(1000);
                assertTrue(notified.get());


                // simple pull test
                Page page = IPFS.pull(connection);
                assertNotNull(page);
                assertEquals(page.peerId(), ipfs.self());
                assertNotNull(page.value());
                assertEquals(page.value().length, 0);
                assertNotNull(page.getEolDate());


                //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
                byte[] output = IPFS.getData(dummySession, cid, new Progress() {
                    @Override
                    public void setProgress(int progress) {
                        LogUtils.verbose(TAG, String.valueOf(progress));
                    }

                    @Override
                    public boolean isCancelled() {
                        return false;
                    }
                });
                assertArrayEquals(input, output);

                connection.close();

                Thread.sleep(1000);

            } finally {
                dummy.clearDatabase();
            }

            Thread.sleep(3000);
            assertEquals(server.numServerConnections(), 0);
        }

    }

}
