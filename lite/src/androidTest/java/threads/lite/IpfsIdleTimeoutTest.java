package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.PeerInfo;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;
import threads.lite.quic.Settings;

@RunWith(AndroidJUnit4.class)
public class IpfsIdleTimeoutTest {


    private static final String TAG = IpfsIdleTimeoutTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void idle_timeout_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        assertEquals(server.numServerConnections(), 0);

        Dummy dummy = Dummy.getInstance(context);

        PeerId host = ipfs.self();
        assertNotNull(host);
        Multiaddr multiaddr = Multiaddr.getLoopbackAddress(ipfs.self(), server.getPort());

        try (Session dummySession = dummy.createSession()) {


            Connection connection = dummySession.dial(multiaddr, Parameters.getDefault());
            Objects.requireNonNull(connection);

            assertEquals(server.numServerConnections(), 1);

            PeerInfo info = dummy.getHost().getPeerInfo(connection);
            assertNotNull(info);
            assertEquals(info.agent(), IPFS.AGENT);

            LogUtils.error(TAG, info.toString());


            Thread.sleep(Settings.MAX_IDLE_TIMEOUT + Settings.MAX_IDLE_RESOLUTION);

            // check if connection is not connected
            assertFalse(connection.isConnected());

            assertEquals(server.numServerConnections(), 0); // also server is not alive anymore
        }

        Thread.sleep(3000);
        assertEquals(server.numServerConnections(), 0);


    }


    @Test
    public void idle_timeout_with_keep_alive_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        assertEquals(server.numServerConnections(), 0);

        Dummy dummy = Dummy.getInstance(context);

        PeerId host = ipfs.self();
        assertNotNull(host);
        Multiaddr multiaddr = Multiaddr.getLoopbackAddress(ipfs.self(), server.getPort());

        try (Session dummySession = dummy.createSession()) {


            Connection connection = dummySession.dial(multiaddr, Parameters.getDefault(true));
            Objects.requireNonNull(connection);

            assertEquals(server.numServerConnections(), 1);

            PeerInfo info = dummy.getHost().getPeerInfo(connection);
            assertNotNull(info);
            assertEquals(info.agent(), IPFS.AGENT);

            LogUtils.error(TAG, info.toString());


            Thread.sleep(Settings.MAX_IDLE_TIMEOUT + Settings.MAX_IDLE_RESOLUTION);

            // check if connection is connected
            assertTrue(connection.isConnected());

            assertEquals(server.numServerConnections(), 1); // also server is alive
        }

        Thread.sleep(3000);
        assertEquals(server.numServerConnections(), 0); // server not alive anymore, session is closed

    }

}
