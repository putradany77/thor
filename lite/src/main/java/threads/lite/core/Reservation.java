package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.quic.Connection;

public record Reservation(@NonNull Limit limit, @NonNull Connection connection,
                          @NonNull Multiaddr relayAddress, @NonNull Multiaddr circuitAddress,
                          long expire) {


    // when 0, there is not limitation of bytes during communication
    public long getLimitData() {
        return limit.data();
    }

    // when 0, there is no time limitation
    public long getLimitDuration() {
        return limit.duration();
    }


    public long expireInMinutes() {
        Date now = new Date();
        long duration = new Date(expire * 1000).getTime() - now.getTime();
        return TimeUnit.MILLISECONDS.toMinutes(duration);
    }


    @NonNull
    @Override
    public String toString() {
        return "Reservation{" +
                " limit=" + limit +
                ", relayAddress=" + relayAddress +
                ", expire=" + expire +
                '}';
    }

    @NonNull
    public PeerId getRelayId() {
        return relayAddress().peerId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return connection.equals(that.connection) &&
                relayAddress.equals(that.relayAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connection, relayAddress); // ok, is invoked
    }


}
