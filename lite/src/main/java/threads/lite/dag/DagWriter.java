package threads.lite.dag;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import merkledag.pb.Merkledag;
import threads.lite.IPFS;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.core.BlockStore;
import threads.lite.core.ReaderInputStream;
import unixfs.pb.Unixfs;


interface DagWriter {
    int DEPTH_REPEAT = 4;
    int LINKS_PER_BLOCK = 1000; // kubo it is 175 [but not big performance gain]


    static long fillNodeLayer(ReaderInputStream reader,
                              BlockStore blockStore,
                              Unixfs.Data.Builder data,
                              Merkledag.PBNode.Builder pbn) throws Exception {
        byte[] bytes = new byte[IPFS.CHUNK_SIZE];
        int numChildren = 0;
        long size = 0L;
        while ((numChildren < LINKS_PER_BLOCK && !reader.done())) {
            int read = reader.read(bytes);
            if (read > 0) {
                if (reader.done() && numChildren == 0) {
                    data.setData(ByteString.copyFrom(bytes, 0, read));
                    size = size + read;
                    break;
                } else {
                    createRawBlock(blockStore, data, pbn, bytes, read);
                    size = size + read;
                }
            }
            numChildren++;
        }
        commit(data, pbn, size);
        return size;
    }

    private static void createRawBlock(BlockStore blockStore,
                                       Unixfs.Data.Builder data,
                                       Merkledag.PBNode.Builder pbn,
                                       byte[] bytes, int read) throws Exception {
        Unixfs.Data.Builder unixfs =
                Unixfs.Data.newBuilder().setType(Unixfs.Data.DataType.Raw)
                        .setFilesize(read)
                        .setData(ByteString.copyFrom(bytes, 0, read));

        Merkledag.PBNode.Builder builder = Merkledag.PBNode.newBuilder();
        builder.setData(ByteString.copyFrom(unixfs.build().toByteArray()));

        Block block = Block.createBlock(builder.build());
        blockStore.storeBlock(block);
        addChild(data, pbn, block.cid(), read);
    }


    static Cid trickle(@NonNull ReaderInputStream reader, @NonNull BlockStore blockStore) throws Exception {
        Result result = fillTrickleRec(reader, blockStore, Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.File)
                .setFilesize(0L), Merkledag.PBNode.newBuilder(), -1);
        return result.cid();
    }


    private static Result fillTrickleRec(@NonNull ReaderInputStream reader,
                                         @NonNull BlockStore blockStore,
                                         Unixfs.Data.Builder data,
                                         Merkledag.PBNode.Builder pbn,
                                         int maxDepth) throws Exception {
        // Always do this, even in the base case
        long size = fillNodeLayer(reader, blockStore, data, pbn);

        for (int depth = 1; maxDepth == -1 || depth < maxDepth; depth++) {
            if (reader.done()) {
                break;
            }

            for (int repeatIndex = 0; repeatIndex < DEPTH_REPEAT && !reader.done(); repeatIndex++) {

                Result result = fillTrickleRec(reader, blockStore, Unixfs.Data.newBuilder()
                        .setType(Unixfs.Data.DataType.Raw)
                        .setFilesize(0L), Merkledag.PBNode.newBuilder(), depth);

                long fileSize = result.size();
                addChild(data, pbn, result.cid(), fileSize);
                size = size + fileSize;
            }
        }
        Block block = commit(data, pbn, size);
        blockStore.storeBlock(block);
        return new Result(block.cid(), size);
    }

    private static Block commit(Unixfs.Data.Builder data,
                                Merkledag.PBNode.Builder pbn,
                                long size) throws Exception {
        data.setFilesize(size);
        pbn.setData(ByteString.copyFrom(data.build().toByteArray()));
        return Block.createBlock(pbn.build());
    }

    private static void addChild(Unixfs.Data.Builder data,
                                 Merkledag.PBNode.Builder pbn,
                                 Cid cid, long fileSize) {
        Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder()
                .setName("")
                .setTsize(fileSize);
        lnb.setHash(ByteString.copyFrom(cid.encoded()));

        pbn.addLinks(lnb.build());
        data.addBlocksizes(fileSize);
    }

    record Result(Cid cid, long size) {
    }
}
