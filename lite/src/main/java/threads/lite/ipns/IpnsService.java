package threads.lite.ipns;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

import ipns.pb.Ipns;
import record.pb.RecordOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cbor.CborObject;
import threads.lite.cbor.Cborable;
import threads.lite.cid.PeerId;
import threads.lite.core.Page;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;


public interface IpnsService {

    String VALIDITY = "Validity";
    String VALIDITY_TYPE = "ValidityType";
    String VALUE = "Value";
    String SEQUENCE = "Sequence";
    String TTL = "TTL";
    String NAME = "Name";


    @SuppressLint("SimpleDateFormat")
    @NonNull
    static Date getDate(@NonNull String format) throws ParseException {
        return Objects.requireNonNull(new SimpleDateFormat(IPFS.TIME_FORMAT_IPFS).parse(format));
    }


    @NonNull
    static Ipns.IpnsRecord create(byte[] key, byte[] value, @NonNull Date eol,
                                  @NonNull Duration duration, @Nullable String name) throws Exception {

        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(eol);

        SortedMap<CborObject, CborObject> map = new TreeMap<>(
                Comparator.comparingInt(Object::hashCode));
        map.put(new CborObject.CborString(VALUE),
                new CborObject.CborByteArray(value));
        map.put(new CborObject.CborString(VALIDITY),
                new CborObject.CborByteArray(format.getBytes()));
        map.put(new CborObject.CborString(VALIDITY_TYPE),
                new CborObject.CborLong(0L));
        map.put(new CborObject.CborString(SEQUENCE),
                new CborObject.CborLong(System.currentTimeMillis()));
        map.put(new CborObject.CborString(TTL),
                new CborObject.CborLong(duration.toNanos()));

        if (name != null && !name.isBlank()) {
            map.put(new CborObject.CborString(NAME),
                    new CborObject.CborString(name));
        }

        CborObject.CborMap cborMap = new CborObject.CborMap(map);

        byte[] data = cborMap.toByteArray();

        Ipns.IpnsRecord.Builder builder = Ipns.IpnsRecord.newBuilder();
        builder.setData(ByteString.copyFrom(data));
        Ipns.IpnsRecord ipnsRecord = builder.buildPartial();
        byte[] sig2 = Key.sign(key, ipnsRecordDataForSigV2(ipnsRecord));
        builder = ipnsRecord.toBuilder();
        builder.setSignatureV2(ByteString.copyFrom(sig2));

        return builder.build();
    }


    @NonNull
    static Page createPage(@NonNull String protocol, @NonNull byte[] data) throws Exception {
        return validate(protocol, RecordOuterClass.Record.parseFrom(data));
    }

    private static byte[] ipnsRecordDataForSigV2(Ipns.IpnsRecord ipnsRecord) {

        byte[] dataForSig = "ipns-signature:".getBytes();

        byte[] data = ipnsRecord.getData().toByteArray();

        ByteBuffer out = ByteBuffer.allocate(dataForSig.length + data.length);
        out.put(dataForSig);
        out.put(data);
        return out.array();
    }

    @NonNull
    static Page validate(@NonNull String protocol,
                         @NonNull RecordOuterClass.Record record) throws Exception {

        PeerId peerId = Key.decodeIpnsKey(record.getKey().toByteArray());

        Ipns.IpnsRecord ipnsRecord = Ipns.IpnsRecord.parseFrom(record.getValue().toByteArray());
        Objects.requireNonNull(ipnsRecord);


        PubKey pubKey = extractPublicKey(peerId, ipnsRecord);
        Objects.requireNonNull(pubKey);

        if (ipnsRecord.hasSignatureV2()) {
            pubKey.verify(ipnsRecordDataForSigV2(ipnsRecord), ipnsRecord.getSignatureV2().toByteArray());
        } else {
            throw new Exception("no valid signature [SigV1 ignored]");
        }

        byte[] data = ipnsRecord.getData().toByteArray();
        if (data.length == 0) {
            throw new Exception("record data is missing");
        }

        CborObject deserialized = CborObject.fromByteArray(data);

        Objects.requireNonNull(deserialized);

        if (deserialized instanceof CborObject.CborMap map) {


            Cborable nd = map.values().get(new CborObject.CborString(VALUE));
            Objects.requireNonNull(nd);
            byte[] value = ((CborObject.CborByteArray) nd.toCbor()).value();

            nd = map.values().get(new CborObject.CborString(VALIDITY));
            Objects.requireNonNull(nd);
            byte[] validity = ((CborObject.CborByteArray) nd.toCbor()).value();
            String date = new String(validity);
            Date eol = getDate(date);

            if (new Date().after(eol)) {
                throw new Exception("outdated ipns record");
            }

            nd = map.values().get(new CborObject.CborString(VALIDITY_TYPE));
            Objects.requireNonNull(nd);
            int type = (int) ((CborObject.CborLong) nd.toCbor()).value();

            if (type != 0) { // 0 is EOL
                throw new Exception("validity type");
            }

            nd = map.values().get(new CborObject.CborString(SEQUENCE));
            Objects.requireNonNull(nd);
            long sequence = ((CborObject.CborLong) nd.toCbor()).value();

            nd = map.values().get(new CborObject.CborString(TTL));
            Objects.requireNonNull(nd);
            long ttl = ((CborObject.CborLong) nd.toCbor()).value();
            if (ttl < 0) {
                throw new Exception("Invalid ttl value");
            }

            String name = null;
            try {
                nd = map.values().get(new CborObject.CborString(NAME));
                if (nd != null) {
                    name = ((CborObject.CborString) nd.toCbor()).value();
                }
            } catch (Throwable throwable) {
                LogUtils.error(IpnsService.class.getSimpleName(), throwable);
            }

            Page page = new Page(peerId, protocol, name, eol.getTime(), value, sequence);

            LogUtils.error(IpnsService.class.getSimpleName(), page.toString()); // todo remove

            return page;
        } else {
            throw new Exception("CborObject.CborMap expected");
        }

    }


    static int compare(@NonNull Page a, @NonNull Page b) {

        long as = a.sequence();
        long bs = b.sequence();

        if (as > bs) {
            return 1;
        } else if (as < bs) {
            return -1;
        }

        Date at = a.getEolDate();
        Date bt = b.getEolDate();

        if (at.after(bt)) {
            return 1;
        } else if (bt.after(at)) {
            return -1;
        }
        return 0;
    }


    // ExtractPublicKey extracts a public key matching `pid` from the IPNS record,
    // if possible.
    //
    // This function returns (nil, nil) when no public key can be extracted and
    // nothing is malformed.
    @NonNull
    private static PubKey extractPublicKey(PeerId pid, Ipns.IpnsRecord ipnsRecord)
            throws Exception {


        if (ipnsRecord.hasPubKey()) {
            byte[] pubKey = ipnsRecord.getPubKey().toByteArray();

            PubKey pk = Key.unmarshalPublicKey(pubKey);
            PeerId expPid = Key.fromPubKey(pk);

            if (!Objects.equals(pid, expPid)) {
                throw new Exception("invalid peer");
            }
            return pk;
        }

        return Key.extractPublicKey(pid);
    }


}
