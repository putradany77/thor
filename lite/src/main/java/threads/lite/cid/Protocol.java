package threads.lite.cid;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.CodedInputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.Objects;
import java.util.Set;

import threads.lite.LogUtils;
import threads.lite.Utils;

public record Protocol(int code, int size, String type,
                       int priority) implements Comparable<Protocol> {

    static final Protocol DNS = new Protocol(53, -1, "dns", 4);
    static final Protocol IP4 = new Protocol(4, 32, "ip4", 2);
    static final Protocol IP6 = new Protocol(41, 128, "ip6", 1);
    static final Protocol DNS4 = new Protocol(54, -1, "dns4", 5);
    static final Protocol DNS6 = new Protocol(55, -1, "dns6", 3);
    static final Protocol DNSADDR = new Protocol(56, -1, "dnsaddr", 6);
    static final Protocol UDP = new Protocol(273, 16, "udp", 7);
    static final Protocol P2P = new Protocol(421, -1, "p2p", 8);
    static final Protocol P2PCIRCUIT = new Protocol(290, 0, "p2p-circuit", 9);
    static final Protocol QUICV1 = new Protocol(461, 0, "quic-v1", 10);

    private static final String IPV4_REGEX = "\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z";

    @NonNull
    static Protocol lookup(String name) {
        return switch (name) {
            case "ip4" -> IP4;

            case "ip6" -> IP6;

            case "dns" -> DNS;

            case "dns4" -> DNS4;

            case "dns6" -> DNS6;

            case "dnsaddr" -> DNSADDR;

            case "udp" -> UDP;

            case "p2p" -> P2P;

            case "p2p-circuit" -> P2PCIRCUIT;

            case "quic-v1" -> QUICV1;

            default -> throw new IllegalArgumentException("unsupported protocol " + name);
        };
    }

    @Nullable
    static Protocol lookup(int code) {
        return switch (code) {
            case 4 -> IP4;
            case 41 -> IP6;
            case 53 -> DNS;
            case 54 -> DNS4;
            case 55 -> DNS6;
            case 56 -> DNSADDR;
            case 273 -> UDP;
            case 421 -> P2P;
            case 290 -> P2PCIRCUIT;
            case 461 -> QUICV1;
            default -> null;
        };
    }


    private static void putUvarint(byte[] buf, long x) {
        int i = 0;
        while (x >= 0x80) {
            buf[i] = (byte) (x | 0x80);
            x >>= 7;
            i++;
        }
        buf[i] = (byte) x;
    }


    static void encodeProtocol(Protocol protocol, OutputStream out) throws IOException {
        int code = protocol.code;
        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(code) + 6) / 7];
        putUvarint(varint, code);
        out.write(varint);
    }

    private static void encodePart(String address, OutputStream outputStream) throws IOException {
        byte[] hashBytes = address.getBytes();
        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(
                hashBytes.length) + 6) / 7];
        putUvarint(varint, hashBytes.length);
        outputStream.write(varint);
        outputStream.write(hashBytes);
    }

    static void encodePart(Integer port, OutputStream outputStream) throws IOException {
        int x = port;
        outputStream.write(new byte[]{(byte) (x >> 8), (byte) x});
    }

    static void encodePart(InetAddress inetAddress, OutputStream outputStream) throws IOException {
        outputStream.write(inetAddress.getAddress());
    }

    static void encodePart(PeerId peerId, OutputStream outputStream) throws IOException {
        byte[] hashBytes = peerId.encoded();

        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(
                hashBytes.length) + 6) / 7];
        putUvarint(varint, hashBytes.length);
        outputStream.write(varint);
        outputStream.write(hashBytes);
    }

    static String getString(CodedInputStream in) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            while (!in.isAtEnd()) {
                Protocol protocol = Protocol.lookup(in.readRawVarint32());
                if (protocol == null) {
                    throw new RuntimeException("invalid address");
                }
                stringBuilder.append("/");
                stringBuilder.append(protocol.type);

                if (protocol.size() == 0)
                    continue;

                Object part = protocol.readPart(in);
                Objects.requireNonNull(part);
                stringBuilder.append("/");
                if (part instanceof InetAddress inetAddress) {
                    stringBuilder.append(inetAddress.getHostAddress());
                } else {
                    stringBuilder.append(part);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(Protocol.class.getSimpleName(), throwable);
        }
        return stringBuilder.toString();
    }

    @Nullable
    static Object getValue(CodedInputStream in, Set<Protocol> values) {

        try {
            while (!in.isAtEnd()) {
                Protocol protocol = Protocol.lookup(in.readRawVarint32());

                if (protocol == null) {
                    return null;
                }

                if (protocol.size() == 0)
                    continue;

                if (values.contains(protocol)) {
                    Object part = protocol.readPart(in);
                    Objects.requireNonNull(part);
                    return part;
                } else {
                    protocol.skipPart(in);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(Protocol.class.getSimpleName(), throwable);
        }
        return null;
    }

    static boolean has(CodedInputStream in, Set<Protocol> values) {

        try {
            while (!in.isAtEnd()) {
                Protocol protocol = Protocol.lookup(in.readRawVarint32());
                if (values.contains(protocol)) {
                    return true;
                }

                if (protocol == null) {
                    throw new RuntimeException("invalid address");
                }

                if (protocol.size() == 0)
                    continue;

                protocol.skipPart(in);
            }
        } catch (Throwable throwable) {
            LogUtils.error(Protocol.class.getSimpleName(), throwable);
        }
        return false;
    }

    static boolean isTransport(Protocol protocol) {
        return switch (protocol.code()) {
            case 4, 53, 41, 54, 55, 56 -> true;
            default -> false;
        };
    }

    static byte[] parseAddress(CodedInputStream in, PeerId peerId) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        boolean isFirst = true;
        boolean dnsAddr = false;
        boolean udpPort = false;
        try {
            while (!in.isAtEnd()) {
                Protocol protocol = Protocol.lookup(in.readRawVarint32());
                if (protocol == null) {
                    return Utils.BYTES_EMPTY;
                }
                if (isFirst) {
                    // check if address is valid and supported
                    if (protocol == DNSADDR) {
                        dnsAddr = true;
                    } else {
                        if (!isTransport(protocol)) {
                            return Utils.BYTES_EMPTY;
                        }
                    }
                    isFirst = false;
                } else {
                    if (protocol == UDP) {
                        udpPort = true;
                    }
                }

                if (protocol.size() == 0) {
                    encodeProtocol(protocol, byteArrayOutputStream);
                    continue;
                }

                Object part = protocol.readPart(in);
                Objects.requireNonNull(part);

                // check if the peer id is not part of the encoded address
                if (protocol == Protocol.P2P &&
                        part instanceof PeerId cmp &&
                        Objects.equals(peerId, cmp)) {
                    // end reached [Note this is hack for address which contains
                    // the /p2p/<peerId> at the end
                    return byteArrayOutputStream.toByteArray();
                } else {
                    encodeProtocol(protocol, byteArrayOutputStream);
                    if (part instanceof PeerId pid) {
                        encodePart(pid, byteArrayOutputStream);
                    }
                    if (part instanceof String address) {
                        encodePart(address, byteArrayOutputStream);
                    }
                    if (part instanceof Integer port) {
                        encodePart(port, byteArrayOutputStream);
                    }
                    if (part instanceof InetAddress inetAddress) {
                        encodePart(inetAddress, byteArrayOutputStream);
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(Protocol.class.getSimpleName(), throwable);
            return Utils.BYTES_EMPTY;
        }
        // check if address has a port, when it is not a dnsAddr
        if (!udpPort && !dnsAddr) {
            return Utils.BYTES_EMPTY; // empty bytes will be returned in case it has no port
        }
        return byteArrayOutputStream.toByteArray();
    }


    @NonNull
    @Override
    public String toString() {
        return type();
    }

    void partToStream(String addr, OutputStream outputStream) throws Exception {

        switch (code()) {
            case 4 -> {
                if (!addr.matches(IPV4_REGEX))
                    throw new IllegalStateException("Invalid IPv4 address: " + addr);
                encodePart(Inet4Address.getByName(addr), outputStream);
            }
            case 41 -> encodePart(Inet6Address.getByName(addr), outputStream);
            case 273 -> encodePart(Integer.parseInt(addr), outputStream);
            case 421 -> encodePart(PeerId.decode(addr), outputStream);
            case 53, 54, 55, 56 -> encodePart(addr, outputStream);
            default -> throw new IllegalStateException("Unknown multiaddr type");
        }

    }

    @NonNull
    private Object readPart(CodedInputStream in) throws Exception {
        int sizeForAddress = sizeForAddress(in);
        byte[] buf;
        switch (code()) {
            case 4, 41 -> {
                buf = in.readRawBytes(sizeForAddress);
                return java.net.InetAddress.getByAddress(buf);
            }
            case 273 -> {
                int a = in.readRawByte() & 0xFF;
                int b = in.readRawByte() & 0xFF;
                return (a << 8) | b;
            }
            case 421 -> {
                buf = in.readRawBytes(sizeForAddress);
                return PeerId.create(buf);
            }
            case 53, 54, 55, 56 -> {
                buf = in.readRawBytes(sizeForAddress);
                return new String(buf);
            }
        }
        throw new IllegalStateException("Unimplemented protocol type: " + type);
    }

    private void skipPart(CodedInputStream in) throws Exception {
        int sizeForAddress = sizeForAddress(in);

        switch (code()) {
            case 4, 41, 421, 53, 54, 55, 56 -> in.skipRawBytes(sizeForAddress);
            case 273 -> in.skipRawBytes(2);
        }
    }

    private int sizeForAddress(CodedInputStream in) throws IOException {
        if (size > 0)
            return size / 8;
        if (size == 0)
            return 0;
        return in.readRawVarint32();
    }

    @Override
    public int compareTo(Protocol o) {
        return Integer.compare(this.priority, o.priority);
    }
}
