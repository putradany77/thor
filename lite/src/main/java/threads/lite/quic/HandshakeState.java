package threads.lite.quic;

enum HandshakeState {

    Initial,
    HasHandshakeKeys,
    HasAppKeys,
    Confirmed;

    public boolean hasNoHandshakeKeys() {
        return ordinal() < HasHandshakeKeys.ordinal();
    }

    public boolean transitionAllowed(HandshakeState proposedState) {
        return this.ordinal() < proposedState.ordinal();
    }

    public boolean isNotConfirmed() {
        return this.ordinal() < Confirmed.ordinal();
    }

}

