package threads.lite.quic;

enum Role {
    Client,
    Server;

    public Role other() {
        return this == Client ? Server : Client;
    }
}
