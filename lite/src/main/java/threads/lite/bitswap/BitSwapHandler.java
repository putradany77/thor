package threads.lite.bitswap;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.DataHandler;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.quic.Stream;

public final class BitSwapHandler implements ProtocolHandler {

    private final Session session;

    public BitSwapHandler(Session session) {
        this.session = session;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.BITSWAP_PROTOCOL), true);
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {
        session.receiveMessage(stream.connection(),
                MessageOuterClass.Message.parseFrom(data));
    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
        LogUtils.error(getClass().getSimpleName(),
                "fin received " + stream.connection().remotePeerId());
    }

}
