package threads.lite.bitswap;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import bitswap.pb.MessageOuterClass;
import merkledag.pb.Merkledag;
import threads.lite.LogUtils;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Multicodec;
import threads.lite.cid.Prefix;
import threads.lite.core.BlockStore;
import threads.lite.dag.DagService;


public record BitSwapMessage(@NonNull Block[] blocks, @NonNull Cid[] haves) {


    public static BitSwapMessage create(MessageOuterClass.Message pbm) throws Exception {
        HashMap<Cid, Block> blocks = new HashMap<>();
        List<Cid> haves = new ArrayList<>();


        // deprecated
        for (ByteString data : pbm.getBlocksList()) {
            // CIDv0, sha256, protobuf only
            Block block = Block.createBlock(Merkledag.PBNode.parseFrom(data.toByteArray()));
            blocks.put(block.cid(), block);
        }

        for (MessageOuterClass.Message.Block payload : pbm.getPayloadList()) {
            ByteString prefix = payload.getPrefix();
            ByteString payloadData = payload.getData();
            Prefix pref = Prefix.decode(prefix.toByteArray());
            byte[] data = payloadData.toByteArray();
            Cid cid = Prefix.sum(pref, data);

            if (cid.getCodec() == Multicodec.RAW) {
                Block block = Block.createBlock(cid, DagService.createRaw(data));
                blocks.put(block.cid(), block);
            } else {
                Block block = Block.createBlock(cid, Merkledag.PBNode.parseFrom(data));
                blocks.put(block.cid(), block);
            }
        }

        for (MessageOuterClass.Message.BlockPresence bi : pbm.getBlockPresencesList()) {
            Cid cid = Cid.decode(bi.getCid().toByteArray());
            Objects.requireNonNull(cid);
            if (!blocks.containsKey(cid)) {
                if (bi.getType() == MessageOuterClass.Message.BlockPresenceType.Have) {
                    haves.add(cid);
                }
            }
        }
        //noinspection SimplifyStreamApiCallChains
        return new BitSwapMessage(
                blocks.values().stream().toArray(Block[]::new),
                haves.stream().toArray(Cid[]::new));
    }


    static void evaluateResponse(@NonNull BlockStore blockstore,
                                 @NonNull MessageOuterClass.Message bsm,
                                 @NonNull Consumer<Cid> cancelConsumer,
                                 @NonNull Consumer<Cid> wantsConsumer,
                                 @NonNull Consumer<Cid> dontHaveConsumer,
                                 @NonNull Consumer<Cid> haveConsumer) {

        for (MessageOuterClass.Message.Wantlist.Entry entry :
                bsm.getWantlist().getEntriesList()) {
            if (entry.getCancel()) {
                cancelConsumer.accept(Cid.decode(entry.getBlock().toByteArray()));
            } else {
                // For each want-have / want-block
                try {
                    Cid cid = Cid.decode(entry.getBlock().toByteArray());
                    if (blockstore.hasBlock(cid)) {

                        boolean isWantBlock =
                                entry.getWantType() == MessageOuterClass.Message.Wantlist.WantType.Block;

                        if (isWantBlock) {
                            // Add BLOCK
                            wantsConsumer.accept(cid);
                        } else {
                            // Add HAVES
                            haveConsumer.accept(cid);
                        }
                    } else {
                        // Only add the task to the queue if the requester wants a DONT_HAVE
                        if (entry.getSendDontHave()) {
                            // Add DONT_HAVEs to the message
                            dontHaveConsumer.accept(cid);
                        }
                    }
                } catch (Throwable throwable) {
                    // Note; CID decode might fail because of unsupported hash type
                    LogUtils.error(BitSwapMessage.class.getSimpleName(),
                            throwable.getClass().getSimpleName() + " : " +
                                    throwable.getMessage());
                }
            }
        }

    }


    @NonNull
    public static MessageOuterClass.Message create(
            MessageOuterClass.Message.Wantlist.WantType wantType, Cid cid) {

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        MessageOuterClass.Message.Wantlist.Builder wantBuilder =
                MessageOuterClass.Message.Wantlist.newBuilder();

        wantBuilder.addEntries(MessageOuterClass.Message.Wantlist.Entry
                .newBuilder()
                .setBlock(ByteString.copyFrom(cid.encoded()))
                .setPriority(1)
                .setCancel(false)
                .setWantType(wantType)
                .setSendDontHave(false)
                .build());

        wantBuilder.setFull(true);
        builder.setWantlist(wantBuilder.build());
        builder.setPendingBytes(0);

        return builder.build();
    }


    @NonNull
    static MessageOuterClass.Message createDontHave(Cid cid) {

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        builder.addBlockPresences(MessageOuterClass.Message.BlockPresence.newBuilder()
                .setType(MessageOuterClass.Message.BlockPresenceType.DontHave)
                .setCid(ByteString.copyFrom(cid.encoded())));
        builder.setPendingBytes(0);

        return builder.build();

    }

    @NonNull
    static MessageOuterClass.Message createHave(Cid cid) {

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        builder.addBlockPresences(MessageOuterClass.Message.BlockPresence.newBuilder()
                .setType(MessageOuterClass.Message.BlockPresenceType.Have)
                .setCid(ByteString.copyFrom(cid.encoded())));
        builder.setPendingBytes(0);

        return builder.build();

    }

    @NonNull
    static MessageOuterClass.Message createBlockMessage(BlockStore blockstore, Cid cid) {

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        byte[] data = blockstore.getBlockData(cid);
        Objects.requireNonNull(data);
        builder.addPayload(MessageOuterClass.Message.Block.newBuilder()
                .setData(ByteString.copyFrom(data))
                .setPrefix(ByteString.copyFrom(cid.getPrefix().encoded())).build());
        builder.setPendingBytes(0);

        return builder.build();

    }

}
