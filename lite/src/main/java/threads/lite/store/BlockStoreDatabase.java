package threads.lite.store;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.lite.cid.Block;
import threads.lite.cid.Cid;

@androidx.room.Database(entities = {Block.class}, version = 2, exportSchema = false)
@TypeConverters({Cid.class})
public abstract class BlockStoreDatabase extends RoomDatabase {

    public abstract BlockStoreDao blockDao();

}
