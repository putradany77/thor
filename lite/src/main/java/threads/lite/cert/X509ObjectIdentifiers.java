package threads.lite.cert;

public interface X509ObjectIdentifiers {

    /**
     * Subject RDN components: telephone_number = 2.5.4.20
     */
    ASN1ObjectIdentifier id_at_telephoneNumber = new ASN1ObjectIdentifier("2.5.4.20").intern();
    /**
     * Subject RDN components: name = 2.5.4.41
     */
    ASN1ObjectIdentifier id_at_name = new ASN1ObjectIdentifier("2.5.4.41").intern();

    ASN1ObjectIdentifier id_at_organizationIdentifier = new ASN1ObjectIdentifier("2.5.4.97").intern();
}
