package threads.lite.cert;

/**
 * DER PrintableString object.
 * <p>
 * X.680 section 37.4 defines PrintableString character codes as ASCII subset of following characters:
 * </p>
 * <ul>
 * <li>Latin capital letters: 'A' .. 'Z'</li>
 * <li>Latin small letters: 'a' .. 'z'</li>
 * <li>Digits: '0'..'9'</li>
 * <li>Space</li>
 * <li>Apostrophe: '\''</li>
 * <li>Left parenthesis: '('</li>
 * <li>Right parenthesis: ')'</li>
 * <li>Plus sign: '+'</li>
 * <li>Comma: ','</li>
 * <li>Hyphen-minus: '-'</li>
 * <li>Full stop: '.'</li>
 * <li>Solidus: '/'</li>
 * <li>Colon: ':'</li>
 * <li>Equals sign: '='</li>
 * <li>Question mark: '?'</li>
 * </ul>
 * <p>
 * Explicit character set escape sequences are not allowed.
 * </p>
 */
public final class DERPrintableString extends ASN1PrintableString {
    /**
     * Basic constructor - this does not validate the string
     */
    public DERPrintableString(String string) {
        super(string);
    }

    DERPrintableString(byte[] contents) {
        super(contents);
    }


}
