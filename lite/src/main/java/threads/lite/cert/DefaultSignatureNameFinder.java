package threads.lite.cert;


import java.util.HashMap;
import java.util.Map;

/**
 * Class for return signature names from OIDs or AlgorithmIdentifiers
 */
final class DefaultSignatureNameFinder {
    private static final Map<ASN1ObjectIdentifier, String> oids = new HashMap<>();

    static {
        //
        // reverse mappings
        //
        oids.put(PKCSObjectIdentifiers.id_RSASSA_PSS, "RSASSA-PSS");

        oids.put(new ASN1ObjectIdentifier("1.2.840.113549.1.1.5"), "SHA1WITHRSA");
        oids.put(PKCSObjectIdentifiers.sha224WithRSAEncryption, "SHA224WITHRSA");
        oids.put(PKCSObjectIdentifiers.sha256WithRSAEncryption, "SHA256WITHRSA");
        oids.put(PKCSObjectIdentifiers.sha384WithRSAEncryption, "SHA384WITHRSA");
        oids.put(PKCSObjectIdentifiers.sha512WithRSAEncryption, "SHA512WITHRSA");


        oids.put(new ASN1ObjectIdentifier("1.2.840.113549.1.1.4"), "MD5WITHRSA");
        oids.put(new ASN1ObjectIdentifier("1.2.840.113549.1.1.2"), "MD2WITHRSA");
        oids.put(new ASN1ObjectIdentifier("1.2.840.10040.4.3"), "SHA1WITHDSA");


    }

    /**
     * Return the signature name for the passed in algorithm identifier. For signatures
     * that require parameters, like RSASSA-PSS, this is the best one to use.
     *
     * @param algorithmIdentifier the AlgorithmIdentifier of interest.
     * @return a string representation of the name.
     */
    static String getAlgorithmName(AlgorithmIdentifier algorithmIdentifier) {
        if (oids.containsKey(algorithmIdentifier.getAlgorithm())) {
            return oids.get(algorithmIdentifier.getAlgorithm());
        }

        return algorithmIdentifier.getAlgorithm().getId();
    }
}
