package threads.magnet.net;

import threads.magnet.metainfo.TorrentId;

public record ConnectionKey(Peer peer, int remotePort, TorrentId torrentId) {

    public Peer getPeer() {
        return peer;
    }

    public TorrentId getTorrentId() {
        return torrentId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ConnectionKey that = (ConnectionKey) obj;
        // must not use peer's port, because it can be updated
        return peer.getInetAddress().equals(that.peer.getInetAddress())
                && remotePort == that.remotePort
                && torrentId.equals(that.torrentId);

    }

    @Override
    public int hashCode() {
        int result = peer.getInetAddress().hashCode();
        result = 31 * result + remotePort;
        result = 31 * result + torrentId.hashCode();
        return result;
    }


}
