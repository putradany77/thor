package threads.magnet.data;

import threads.magnet.metainfo.Torrent;

public record DataDescriptorFactory(ChunkVerifier verifier) {

    public DataDescriptor createDescriptor(Torrent torrent, Storage storage) {
        return new DataDescriptor(storage, torrent, verifier);
    }
}
