package threads.magnet.data.range;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import threads.magnet.net.buffer.ByteBufferView;

public record SynchronizedRange(Range delegate, ReadWriteLock lock) implements Range {


    /**
     * Create a data range synchronized with a private lock.
     *
     * @since 1.2
     */
    public static SynchronizedRange createSynchronizedRange(Range delegate) {
        return new SynchronizedRange(delegate, new ReentrantReadWriteLock());
    }


    /**
     * {@inheritDoc}
     *
     * @since 1.2
     */
    @Override
    public long length() {
        return delegate.length();
    }

    /**
     * {@inheritDoc}
     * <p>
     * Child subrange shares the same lock as its' parent range.
     *
     * @since 1.2
     */
    @Override
    public SynchronizedRange getSubrange(long offset, long length) {
        return new SynchronizedRange(delegate.getSubrange(offset, length), lock);
    }

    /**
     * {@inheritDoc}
     * <p>
     * Child subrange shares the same lock as its' parent range.
     *
     * @since 1.2
     */
    @Override
    public SynchronizedRange getSubrange(long offset) {
        return new SynchronizedRange(delegate.getSubrange(offset), lock);
    }

    /**
     * {@inheritDoc}
     * <p>
     * Blocks current thread if there are concurrent write operations in progress.
     * Blocks all concurrent write operations.
     *
     * @since 1.2
     */
    @Override
    public byte[] getBytes() {
        lock.readLock().lock();
        try {
            return delegate.getBytes();
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public boolean getBytes(ByteBuffer buffer) {
        lock.readLock().lock();
        try {
            return delegate.getBytes(buffer);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Blocks current thread if there are concurrent read or write operations in progress.
     * Block all concurrent read or write operations.
     *
     * @since 1.2
     */
    @Override
    public void putBytes(byte[] block) {
        lock.writeLock().lock();
        try {
            delegate.putBytes(block);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public void putBytes(ByteBufferView buffer) {
        lock.writeLock().lock();
        try {
            delegate.putBytes(buffer);
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * @since 1.3
     */
    public ReadWriteLock getLock() {
        return lock;
    }


    public Range getDelegate() {
        return delegate;
    }
}
