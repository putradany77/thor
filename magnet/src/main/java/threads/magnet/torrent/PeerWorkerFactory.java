package threads.magnet.torrent;

import threads.magnet.net.ConnectionKey;

public record PeerWorkerFactory(MessageRouter router) {

    public PeerWorker createPeerWorker(ConnectionKey connectionKey) {
        return RoutingPeerWorker.create(connectionKey, router);
    }
}
