package threads.magnet.torrent;

import java.time.Duration;
import java.util.function.Consumer;

import threads.magnet.protocol.Choke;
import threads.magnet.protocol.Message;
import threads.magnet.protocol.Unchoke;

interface Choker {
    Duration CHOKING_THRESHOLD = Duration.ofMillis(10000);


    /**
     * Inspects connection state and yields choke/unchoke messages when appropriate.
     *
     * @param connectionState Connection state for the choker
     *                        to inspect and update choked/unchoked status.
     * @param messageConsumer Message worker
     * @since 1.0
     */
    static void handleConnection(ConnectionState connectionState, Consumer<Message> messageConsumer) {

        Boolean shouldChokeOptional = connectionState.getShouldChoke();
        boolean choking = connectionState.isChoking();
        boolean peerInterested = connectionState.isPeerInterested();

        if (shouldChokeOptional == null) {
            if (peerInterested && choking) {
                if (mightUnchoke(connectionState)) {
                    shouldChokeOptional = Boolean.FALSE; // should unchoke
                }
            } else if (!peerInterested && !choking) {
                shouldChokeOptional = Boolean.TRUE;
            }
        }

        if (shouldChokeOptional != null) {
            if (shouldChokeOptional != choking) {
                if (shouldChokeOptional) {
                    // choke immediately
                    connectionState.setChoking(true);
                    messageConsumer.accept(Choke.instance());
                    connectionState.setLastChoked(System.currentTimeMillis());
                } else if (mightUnchoke(connectionState)) {
                    connectionState.setChoking(false);
                    messageConsumer.accept(Unchoke.instance());
                }
            }
        }
    }

    private static boolean mightUnchoke(ConnectionState connectionState) {
        // unchoke depending on last choked time to avoid fibrillation
        return System.currentTimeMillis() - connectionState.getLastChoked() >= CHOKING_THRESHOLD.toMillis();
    }
}
