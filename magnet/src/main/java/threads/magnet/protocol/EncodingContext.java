package threads.magnet.protocol;

import threads.magnet.net.Peer;

public record EncodingContext(Peer peer) {

}
