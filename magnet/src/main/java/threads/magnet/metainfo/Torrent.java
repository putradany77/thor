package threads.magnet.metainfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;

import threads.magnet.LogUtils;


public record Torrent(@NonNull TorrentId torrentId, @NonNull String name,
                      @NonNull TorrentSource source, @NonNull List<TorrentFile> files,
                      byte[] chunkHashes, long size, long chunkSize, boolean isPrivate,
                      long creationDate, @Nullable String createdBy) {
    private static final String TAG = Torrent.class.getSimpleName();

    private static final int CHUNK_HASH_LENGTH = 20;


    static Torrent createTorrent(@NonNull TorrentId torrentId,
                                 @NonNull String name,
                                 @NonNull TorrentSource source,
                                 @NonNull List<TorrentFile> torrentFiles,
                                 byte[] chunkHashes,
                                 long size,
                                 long chunkSize,
                                 boolean isPrivate,
                                 long creationDate, @Nullable String createdBy) {

        if (chunkHashes.length % CHUNK_HASH_LENGTH != 0) {
            throw new RuntimeException("Invalid chunk hashes string -- length (" + chunkHashes.length
                    + ") is not divisible by " + CHUNK_HASH_LENGTH);
        }
        if (torrentFiles.isEmpty()) {
            // TODO: Name can be missing according to the spec,
            // so need to make sure that it's present
            // (probably by setting it to a user-defined value after processing the threads.torrent metainfo)
            torrentFiles.add(TorrentFile.createTorrentFile(size, Collections.singletonList(name)));
        }

        Torrent torrent = new Torrent(torrentId, name, source, torrentFiles, chunkHashes, size,
                chunkSize, isPrivate, creationDate, createdBy);
        torrent.build();
        LogUtils.info(TAG, torrent.toString());
        return torrent;
    }

    @NonNull
    @Override
    public String toString() {
        return "Torrent{" +
                "source=" + source +
                ", torrentId=" + torrentId +
                ", name='" + name + '\'' +
                ", chunkSize=" + chunkSize +
                ", chunkHashes=" + Arrays.toString(chunkHashes) +
                ", size=" + size +
                ", files=" + files +
                ", creationDate=" + creationDate +
                ", isPrivate=" + isPrivate +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Torrent that = (Torrent) o;
        return chunkSize == that.chunkSize &&
                size == that.size &&
                isPrivate == that.isPrivate &&
                Objects.equals(torrentId, that.torrentId) &&
                Objects.equals(name, that.name) &&
                Arrays.equals(chunkHashes, that.chunkHashes) &&
                Objects.equals(files, that.files) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(createdBy, that.createdBy);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(torrentId, name, chunkSize, size, files, isPrivate, creationDate, createdBy);
        result = 31 * result + Arrays.hashCode(chunkHashes);
        return result;
    }

    public TorrentSource getSource() {
        return source;
    }

    public TorrentId getTorrentId() {
        return torrentId;
    }

    public String getName() {
        return name;
    }

    public long getChunkSize() {
        return chunkSize;
    }

    public Iterable<byte[]> getIterableChunkHashes() {

        return () -> new Iterator<>() {

            private int read;

            @Override
            public boolean hasNext() {
                return read < chunkHashes.length;
            }

            @Override
            public byte[] next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                int start = read;
                read += CHUNK_HASH_LENGTH;
                return Arrays.copyOfRange(chunkHashes, start, read);
            }
        };
    }

    public long getSize() {
        return size;
    }

    public List<TorrentFile> getFiles() {
        return Collections.unmodifiableList(files);
    }

    public boolean isPrivate() {
        return isPrivate;
    }


    private void build() {

        Map<Long, TorrentFile> ranges = new HashMap<>();
        List<TorrentFile> files = getFiles();
        long startRange = 0L;
        for (TorrentFile file : files) {

            LogUtils.info(TAG, "start " + startRange + " " + file.toString());
            ranges.put(startRange, file);
            startRange += file.getSize();
        }

        long chunkSize = this.chunkSize;


        int chunks = 0;
        long off, lim;
        long remaining = size;
        while (remaining > 0) {
            off = chunks * chunkSize;
            lim = Math.min(chunkSize, remaining);

            List<TorrentFile> chunkFiles = new ArrayList<>();
            for (Map.Entry<Long, TorrentFile> e : ranges.entrySet()) {
                long start = e.getKey();
                if (start <= (lim + off)) {
                    TorrentFile entry = e.getValue();
                    Objects.requireNonNull(entry);
                    if (off <= start + entry.getSize()) {
                        chunkFiles.add(entry);
                    }
                }
            }
            for (TorrentFile tf : chunkFiles) {
                tf.addPiece(chunks);
            }

            chunks++;

            remaining -= chunkSize;
        }
    }
}
