package threads.magnet.metainfo;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import threads.magnet.LogUtils;
import threads.magnet.bencoding.BEInteger;
import threads.magnet.bencoding.BEList;
import threads.magnet.bencoding.BEMap;
import threads.magnet.bencoding.BEObject;
import threads.magnet.bencoding.BEParser;
import threads.magnet.bencoding.BEString;
import threads.magnet.bencoding.BEType;
import threads.magnet.service.CryptoUtil;


public interface MetadataService {
    String TAG = MetadataService.class.getSimpleName();
    // not yet used private static final String ANNOUNCE_KEY = "announce";
    // not yet used private static final String ANNOUNCE_LIST_KEY = "announce-list";
    String INFOMAP_KEY = "info";
    String TORRENT_NAME_KEY = "name";
    String CHUNK_SIZE_KEY = "piece length";
    String CHUNK_HASHES_KEY = "pieces";
    String TORRENT_SIZE_KEY = "length";
    String FILES_KEY = "files";
    String FILE_SIZE_KEY = "length";
    String FILE_PATH_ELEMENTS_KEY = "path";
    String PRIVATE_KEY = "private";
    String CREATION_DATE_KEY = "creation date";
    String CREATED_BY_KEY = "created by";


    static Torrent fromByteArray(byte[] bs) {
        return buildTorrent(bs);
    }


    private static Torrent buildTorrent(byte[] bs) {
        try (BEParser parser = BEParser.create(bs)) {
            if (parser.readType() != BEType.MAP) {
                throw new RuntimeException("Invalid metainfo format -- expected a map, got: "
                        + parser.readType().name().toLowerCase());
            }

            BEMap metadata = parser.readMap();
            BEMap infoDictionary;
            Map<String, BEObject> root = metadata.value();
            if (root.containsKey(INFOMAP_KEY)) {
                // standard BEP-3 format
                infoDictionary = (BEMap) root.get(INFOMAP_KEY);
            } else {
                // BEP-9 exchanged metadata (just the info dictionary)
                infoDictionary = metadata;
            }
            Objects.requireNonNull(infoDictionary);
            TorrentSource source = infoDictionary::content;


            TorrentId torrentId = TorrentId.fromBytes(CryptoUtil.getSha1Digest(
                    infoDictionary.content()));

            Map<String, BEObject> infoMap = infoDictionary.value();

            String name = "";
            if (infoMap.get(TORRENT_NAME_KEY) != null) {
                byte[] data = ((BEString) Objects.requireNonNull(infoMap.get(TORRENT_NAME_KEY))).getValue();
                name = new String(data, StandardCharsets.UTF_8);
            }

            BigInteger chunkSize = ((BEInteger) Objects.requireNonNull(infoMap.get(CHUNK_SIZE_KEY))).getValue();


            byte[] chunkHashes = ((BEString) Objects.requireNonNull(infoMap.get(CHUNK_HASHES_KEY))).getValue();


            List<TorrentFile> torrentFiles = new ArrayList<>();
            long size;
            if (infoMap.get(TORRENT_SIZE_KEY) != null) {
                BigInteger torrentSize = ((BEInteger) Objects.requireNonNull(infoMap.get(TORRENT_SIZE_KEY))).getValue();
                size = torrentSize.longValue();

            } else {
                List<BEObject> files =
                        ((BEList) Objects.requireNonNull(infoMap.get(FILES_KEY))).value();
                BigInteger torrentSize = BigInteger.ZERO;
                for (BEObject object : files) {
                    BEMap file = (BEMap) object;
                    Map<String, BEObject> fileMap = file.value();

                    BigInteger fileSize = ((BEInteger) Objects.requireNonNull(fileMap.get(FILE_SIZE_KEY))).getValue();

                    torrentSize = torrentSize.add(fileSize);

                    List<BEObject> objectList =
                            ((BEList) Objects.requireNonNull(fileMap.get(FILE_PATH_ELEMENTS_KEY)))
                                    .value();
                    List<BEString> pathElements = new ArrayList<>();
                    objectList.forEach(beObject -> pathElements.add((BEString) beObject));


                    torrentFiles.add(TorrentFile.createTorrentFile(
                            fileSize.longValue(), pathElements.stream()
                                    .map(bytes -> bytes.getValue(StandardCharsets.UTF_8))
                                    .collect(Collectors.toList())));
                }

                size = torrentSize.longValue();
            }
            boolean isPrivate = false;
            if (infoMap.get(PRIVATE_KEY) != null) {
                if (BigInteger.ONE.equals(
                        ((BEInteger) Objects.requireNonNull(infoMap.get(PRIVATE_KEY))).getValue())) {
                    isPrivate = true;
                }
            }
            long creationDate = System.currentTimeMillis();
            if (root.get(CREATION_DATE_KEY) != null) {

                // TODO: some torrents contain bogus values here (like 101010101010), which causes an exception
                try {
                    BigInteger epochMilli = ((BEInteger) Objects.requireNonNull(
                            root.get(CREATION_DATE_KEY))).getValue();
                    creationDate = epochMilli.intValue();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
            String createdBy = null;

            if (root.get(CREATED_BY_KEY) != null) {
                byte[] created = ((BEString) Objects.requireNonNull(root.get(CREATED_BY_KEY))).getValue();
                createdBy = new String(created, StandardCharsets.UTF_8);
            }

             /* Not yet used anymore
            if (root.containsKey(ANNOUNCE_LIST_KEY)){

                BEList announceList = (BEList) root.get(ANNOUNCE_LIST_KEY);
                assert announceList != null;

                //noinspection unchecked
                List<BEList> tierList = (List<BEList>) announceList.getValue();
                List<List<String>> trackerUrls = new ArrayList<>(tierList.size() + 1);
                for (BEList tierElement : tierList) {

                    List<String> tierTackerUrls;

                    //noinspection unchecked
                    List<BEString> trackerUrlList = (List<BEString>) tierElement.getValue();
                    tierTackerUrls = new ArrayList<>(trackerUrlList.size() + 1);
                    for (BEString trackerUrlElement : trackerUrlList) {
                        tierTackerUrls.add(trackerUrlElement.getValue(defaultCharset));
                    }
                    trackerUrls.add(tierTackerUrls);
                }

            } else if (root.containsKey(ANNOUNCE_KEY)) {
                byte[] trackerUrl = (byte[]) Objects.requireNonNull(root.get(ANNOUNCE_KEY)).getValue();
                LogUtils.error(TAG, new String(trackerUrl));
            } */


            return Torrent.createTorrent(torrentId, name, source, torrentFiles,
                    chunkHashes, size, chunkSize.longValue(), isPrivate, creationDate, createdBy);

        } catch (Exception e) {
            throw new RuntimeException("Invalid metainfo format", e);
        }
    }
}
